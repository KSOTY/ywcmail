/*
    ywcmail -- simple pop3/smtp web e-mail cgi
    Copyright (C) 2002  Chan Yin Chi < 0330@bigfoot.com >
    code based on null webmail
    Additional Code Copyright (C) 2016 < kt3@gmx.co.uk > 
 
*/

/* FUNCTION LIST
 * void send_header(int cacheable, int status, char *title, char *extra_header, char *mime_type, int length, time_t mod);
 * void wmcookieget();
 * int  wmcookieset();
 * void wmcookiekill();
 * int  printhex(const char *format, ...);
 * int  printht(const char *format, ...);
 * void printline(char *msgtext);
 * int  EncodeBase64(char *src, int srclen);
 * int  DecodeBase64(char *src);
 * char *DecodeRFC2047(char *src);
 * int DecodeHTML(short int reply, char *src, char *ctype, short int crlf);
 * int  DecodeQP(char *src);
 * int DecodeText(short int reply, char *src);
 * char *wmgetdate(char *src);
 * wmheader *webmailheader();
 * void webmailraw();
 * void webmailfiledl();
 * char *webmailfileul(char *xfilename, char *xfilesize);
 * int  retardedOEmime(int reply, char *ctype);
 * void webmailmime(wmheader *header, int nummessage, int reply);
 * void webmaillist();
 * void webmailread();
 * void webmailwrite();
 * void webmailsend();
 * void webmaildelete();
 */
#include "main.h"

static const char Base64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void send_header(int cacheable, int status, char *title, char *extra_header, char *mime_type, int length, time_t mod)
{
	char timebuf[100];
	static short int sent=0;
	time_t t;

	if (sent) return;
	t=time((time_t*)0);
	if (length>=0) {
		printf("Content-Length: %d\r\n", length);
	}
	if (mod!=(time_t)-1) {
		strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&mod));
		printf("Last-Modified: %s\r\n", timebuf);
	}
	if (cacheable) {
		printf("Cache-Control: public\r\n");
		printf("Pragma: public\r\n");
	} else {
		strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&t));
		printf("Expires: %s\r\n", timebuf);
		printf("Cache-Control: no-store\r\n");
		printf("Pragma: no-cache\r\n");
	}
	if (extra_header!=(char*)0) {
		printf("Content-Type: %s\r\n", mime_type);
	} else {
		printf("Content-Type: text/html\r\n");
	}
	printf("\r\n");
	sent=1;
	return;
}

/* see http://www.netscape.com/newsref/std/cookie_spec.html for cookie handling stuff */
void wmcookieget()
{
	char timebuffer[100];
	char *ptemp;
	time_t t;

	memset(wmusername, 0, sizeof(wmusername));
	memset(wmpassword, 0, sizeof(wmpassword));
	memset(wmservertype, 0, sizeof(wmservertype));
	memset(wmpop3server, 0, sizeof(wmpop3server));
	memset(wmsmtpserver, 0, sizeof(wmsmtpserver));
	ptemp=strcasestr(request.Cookie, "wminfo=");
	if (ptemp==NULL) return;
	ptemp+=7;
	while ((*ptemp)&&(*ptemp!=':')&&(*ptemp!=';')&&(strlen(wmusername)<sizeof(wmusername)))
		wmusername[strlen(wmusername)]=*ptemp++;
	ptemp++;
	while ((*ptemp)&&(*ptemp!=':')&&(*ptemp!=';')&&(strlen(wmservertype)<sizeof(wmservertype)-1))
		wmservertype[strlen(wmservertype)]=*ptemp++;
#ifndef SUPPORT_IMAP
	snprintf(wmservertype, sizeof(wmservertype)-1, "POP3");
#endif
#ifndef SUPPORT_POP3
	snprintf(wmservertype, sizeof(wmservertype)-1, "IMAP");
#endif
#ifdef NO_USER_HOSTS
	snprintf(wmpop3server, sizeof(wmpop3server)-1, "%s", POP3_HOST);
	snprintf(wmsmtpserver, sizeof(wmsmtpserver)-1, "%s", SMTP_HOST);
#else
	ptemp++;
	while ((*ptemp)&&(*ptemp!=':')&&(*ptemp!=';')&&(strlen(wmpop3server)<sizeof(wmpop3server)-1))
		wmpop3server[strlen(wmpop3server)]=*ptemp++;
	ptemp++;
	while ((*ptemp)&&(*ptemp!=':')&&(*ptemp!=';')&&(strlen(wmsmtpserver)<sizeof(wmsmtpserver)-1))
		wmsmtpserver[strlen(wmsmtpserver)]=*ptemp++;
#endif
	ptemp=strcasestr(request.Cookie, "wmpass=");
	if (ptemp==NULL) return;
	ptemp+=7;
	while ((*ptemp)&&(*ptemp!=';')&&(strlen(wmpassword)<sizeof(wmpassword)))
		wmpassword[strlen(wmpassword)]=*ptemp++;
	t=time((time_t*)0)+604800;
	strftime(timebuffer, sizeof(timebuffer), RFC1123FMT, gmtime(&t));
	printf("Set-Cookie: wminfo=%s:%s:%s:%s; path=%s/; expires=%s\r\n", wmusername, wmservertype, wmpop3server, wmsmtpserver, request.ScriptName, timebuffer);
	printf("Set-Cookie: wmpass=%s; path=%s/\r\n", wmpassword, request.ScriptName);
}

int wmcookieset()
{
	time_t t;
	char timebuffer[100];

	if (strcasecmp(request.RequestMethod, "POST")==0) {
		if (getpostenv("WMUSERNAME")!=NULL) {
			strncpy(wmusername, getpostenv("WMUSERNAME"), sizeof(wmusername)-1);
		}
		if (getpostenv("WMPASSWORD")!=NULL) {
			strncpy(wmpassword, getpostenv("WMPASSWORD"), sizeof(wmpassword)-1);
		}
		if (getpostenv("WMSERVERTYPE")!=NULL) {
			strncpy(wmservertype, getpostenv("WMSERVERTYPE"), sizeof(wmservertype)-1);
		}
#ifndef SUPPORT_IMAP
		snprintf(wmservertype, sizeof(wmservertype)-1, "POP3");
#endif
#ifndef SUPPORT_POP3
		snprintf(wmservertype, sizeof(wmservertype)-1, "IMAP");
#endif
#ifdef NO_USER_HOSTS
		snprintf(wmpop3server, sizeof(wmpop3server)-1, "%s", POP3_HOST);
		snprintf(wmsmtpserver, sizeof(wmsmtpserver)-1, "%s", SMTP_HOST);
#else
		if (getpostenv("WMPOP3SERVER")!=NULL) {
			strncpy(wmpop3server, getpostenv("WMPOP3SERVER"), sizeof(wmpop3server)-1);
		}
		if (getpostenv("WMSMTPSERVER")!=NULL) {
			strncpy(wmsmtpserver, getpostenv("WMSMTPSERVER"), sizeof(wmsmtpserver)-1);
		}
#endif
	}
	/* thanks to jberkes@pc-tools.net for the syslog addition */
	if (wmserver_connect()==0) {
		t=time((time_t*)0)+604800;
		strftime(timebuffer, sizeof(timebuffer), RFC1123FMT, gmtime(&t));
		printf("Set-Cookie: wminfo=%s:%s:%s:%s; path=%s/; expires=%s\r\n", wmusername, wmservertype, wmpop3server, wmsmtpserver, request.ScriptName, timebuffer);
		printf("Set-Cookie: wmpass=%s; path=%s/\r\n", wmpassword, request.ScriptName);
#ifndef WIN32
#ifdef USE_SYSLOG
		syslog(LOG_MAIL|LOG_INFO, "%s - %s @ %s success", request.ClientIP, wmusername, wmpop3server);
#endif
#endif
		return 0;
	}
#ifndef WIN32
#ifdef USE_SYSLOG
	syslog(LOG_MAIL|LOG_INFO, "%s - Invalid username or password for %s @ %s", request.ClientIP, wmusername, wmpop3server);
#endif
#endif
	wmexit();
	return -1;
}

void wmcookiekill()
{
	char timebuffer[100];
	time_t t;

	t=time((time_t*)0)+604800;
	strftime(timebuffer, sizeof(timebuffer), RFC1123FMT, gmtime(&t));
	printf("Set-Cookie: wmpass=; path=%s/\r\n", request.ScriptName);
	return;
}

int printhex(const char *format, ...)
{
	char *hex="0123456789ABCDEF";
	unsigned char buffer[1024];
	int offset=0;
	va_list ap;

	va_start(ap, format);
	vsnprintf(buffer, sizeof(buffer)-1, format, ap);
	va_end(ap);
	while (buffer[offset]) {
		if ((buffer[offset]>32)&&(buffer[offset]<128)&&(buffer[offset]!='<')&&(buffer[offset]!='>')) {
			printf("%c", buffer[offset]);
		} else {
			printf("%%%c%c", hex[(unsigned int)buffer[offset]/16], hex[(unsigned int)buffer[offset]&15]);
		}
		offset++;
	}
	return 0;
}

int printht(const char *format, ...)
{
	unsigned char buffer[1024];
	int offset=0;
	va_list ap;

	va_start(ap, format);
	vsnprintf(buffer, sizeof(buffer)-1, format, ap);
	va_end(ap);
	while (buffer[offset]) {
		if (buffer[offset]=='<') {
			printf("&lt;");
		} else if (buffer[offset]=='>') {
			printf("&gt;");
		} else if (buffer[offset]=='&') {
			printf("&amp;");
		} else {
			printf("%c", buffer[offset]);
		}
		offset++;
	}
	return 0;
}

void printline(short int reply, char *msgtext)
{
	char *pTemp;
	char *pTemp2;
	char line[120];

	pTemp=msgtext;
	while (strlen(pTemp)>80) {
		memset(line, 0, sizeof(line));
		snprintf(line, 80, "%s", pTemp);
		pTemp2=strrchr(line, ' ');
		if (pTemp2!=NULL) *pTemp2='\0';
		if (reply) printf("> ");
		printht("%s\r\n", line);
		pTemp+=strlen(line);
		if (pTemp2!=NULL) pTemp+=1;
	}
	if (reply) printf("> ");
	printht("%s\r\n", pTemp);
}

char *wmgetdate(char *src)
{
	char *months[]={
		"Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	};
	char *pTemp=src;
	int i;
	int stat;
	int day, month, year;
	static char newdate[16];

	memset(newdate, 0, sizeof(newdate));
	stat=-1;
	while ((*pTemp)&&(!isdigit(*pTemp))) pTemp++;
	if(!isdigit(*pTemp)) return "";
	if (*pTemp=='0') pTemp++;
	day=atoi(pTemp);
	while ((*pTemp)&&(!isalpha(*pTemp))) pTemp++;
	stat=-1;
	for (i=0;i<12;i++) {
		if (strncasecmp(pTemp, months[i], 3)==0) stat=i;
	}
	if (stat==-1) return "";
	month=stat;
	while ((*pTemp)&&(!isdigit(*pTemp))) pTemp++;
	year=atoi(pTemp);
	if ((day)&&(year)) {
		snprintf(newdate, 20, "%s %02d, %04d", months[month], day, year);
	}
	return newdate;
}

char *DecodeRFC2047(char *src)
{
	static char dest[1024];
	char *pos;
	int destidx;
	int state;
	int ch=0;
	int szdest;

	memset(dest, 0, sizeof(dest));
	szdest=sizeof(dest)-1;
	destidx=0;
	while (*src) {
		if (strncmp(src, "=?", 2)!=0) {
			dest[destidx++]=*src++;
			continue;
		}
		while ((*src)&&(*src!='?')) src++;
		if (*src=='\0') return dest;
		src++;
		// we don't try to make sense of the charset
		while ((*src)&&(*src!='?')) src++;
		if (*src=='\0') return dest;
		src++;
		if (tolower(*src)=='q') {
			while ((*src)&&(*src!='?')) src++;
			if (*src=='\0') return dest;
			src++;
			while ((ch=*src++)!='\0') {
				if (ch=='?') break;
				if (ch=='=') {
					if (isxdigit(src[0])&&isxdigit(src[1])) {
						dest[destidx++]=(char)IntFromHex(src);
						src+=2;
					}
				} else {
					dest[destidx++]=ch;
				}
				if (destidx>=szdest) return dest;
			}
		} else if (tolower(*src)=='b') {
			while ((*src)&&(*src!='?')) src++;
			if (*src=='\0') return dest;
			src++;
			state=0;
			while ((ch=*src++)!='\0') {
				if (isspace(ch)) continue;
				if (ch=='=') break;
				pos=strchr(Base64, ch);
				if (pos==0) break;
				switch (state) {
				case 0:
					if (dest) {
						if (destidx>=szdest) return dest;
						dest[destidx]=(pos-Base64)<<2;
					}
					state=1;
					break;
				case 1:
					if (dest) {
						if (destidx+1>=szdest) return dest;
						dest[destidx]|=(pos-Base64)>>4;
						dest[destidx+1]=((pos-Base64)&0x0f)<<4;
					}
					destidx++;
					state=2;
					break;
				case 2:
					if (dest) {
						if (destidx+1>=szdest) return dest;
						dest[destidx]|=(pos-Base64)>>2;
						dest[destidx+1]=((pos-Base64)&0x03)<<6;
					}
					destidx++;
					state=3;
					break;
				case 3:
					if (dest) {
						if (destidx>=szdest) return dest;
						dest[destidx]|=(pos-Base64);
					}
					destidx++;
					state=0;
					break;
				}
			}
		}
		if (ch=='=') {
			while ((*src)&&(*src!='?')) src++;
		}
		if (*src=='\0') return dest;
		src++;
		if (*src=='=') src++;
		ch=0;
	}
	return dest;
}

int DecodeHTML(short int reply, char *src, char *ctype, short int crlf)
{
	char dest[1024];
	char *destidx;

	if (strncasecmp(ctype, "text/html", 9)!=0) return 0;
	memset(dest, 0, sizeof(dest));
	destidx=dest;
//	if (reply) printf("> ");
	while ((*src)&&(strlen(dest)<sizeof(dest))) {
		if (strncasecmp(src, "<SCRIPT", 7)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<EMBED", 6)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<HEAD", 5)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<HTML", 5)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<META", 5)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<LINK", 5)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<OBJECT", 7)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<PARAM", 6)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (strncasecmp(src, "<TITLE", 6)==0) {
			while ((*src)&&(*src!='>')) src++;
			if (*src) src++;
		} else if (reply) {
			if ((strncasecmp(src, "<BR>", 4)==0)||(strncasecmp(src, "<P>", 3)==0)||(strncasecmp(src, "<TR", 3)==0)) {
				while ((*src)&&(*src!='>')) src++;
				if (*src) src++;
				printf("%s\r\n", dest);
				if (reply) printf("> ");
				memset(dest, 0, sizeof(dest));
				destidx=dest;
			} else if (strncasecmp(src, "<LI>", 4)==0) {
				while ((*src)&&(*src!='>')) src++;
				if (*src) src++;
				printf("%s\r\n", dest);
				if (reply) printf("> ");
				printf("* ");
				memset(dest, 0, sizeof(dest));
				destidx=dest;
			} else if (strncasecmp(src, "<", 1)==0) {
				while ((*src)&&(*src!='>')) src++;
				if (*src) src++;
			} else {
				*destidx++=*src++;
			}
		} else {
			*destidx++=*src++;
		}
	}
	if (strlen(dest)>0) {
//		if (reply) printf("> ");
		printf("%s", dest);
		if (crlf) printf("\r\n");
	}
	return (strlen(dest));
}

int DecodeQP(short int reply, char *src, char *ctype)
{
	char dest[1024];
	char *destidx;
	short int crlf=1;

	memset(dest, 0, sizeof(dest));
	destidx=dest;
	if ((strncasecmp(wmservertype, "pop3", 4)==0)&&(src[0]=='.')) src++;
	while ((*src)&&(strlen(dest)<sizeof(dest))) {
		if (*src=='=') {
			src++;
			if (*src==0) {
				crlf=0;
			} else if (isxdigit(src[0])&&isxdigit(src[1])) {
				*destidx++=(char)IntFromHex(src);
				src+=2;
			}
		} else if ((*src=='\r')||(*src=='\n')) {
			src++;
		} else {
			*destidx++=*src++;
		}
	}
	if (strncasecmp(ctype, "text/html", 9)==0) {
		DecodeHTML(reply, dest, ctype, crlf);
	} else {
		if (reply) printf("> ");
		printf("%s\r\n", dest);
	}
	return (strlen(dest));
}

int DecodeText(short int reply, char *src)
{
	char dest[1024];
	char *destidx;

	memset(dest, 0, sizeof(dest));
	destidx=dest;
	if ((strncasecmp(wmservertype, "pop3", 4)==0)&&(src[0]=='.')) src++;
	while ((*src)&&(strlen(dest)<sizeof(dest)-1)) {
		if (*src=='\r') {
			*destidx++=*src++;
			if (*src!='\n') *destidx++='\n';
		} else {
			*destidx++=*src++;
		}
	}
	if (reply) printf("> ");
	printf("%s\r\n", dest);
	return (strlen(dest));
}

int EncodeBase64(char *src, int srclen)
{
	unsigned char a, b, c, d, *cp;
	int dst, i, enclen, remlen, linelen;

	cp=src;
	dst=0;
	linelen=0;
	enclen=srclen/3;
	remlen=srclen-3*enclen;
	for (i=0;i<enclen;i++) {
		a=(cp[0]>>2);
	        b=(cp[0]<<4)&0x30;
		b|=(cp[1]>>4);
		c=(cp[1]<<2)&0x3c;
		c|=(cp[2]>>6);
		d=cp[2]&0x3f;
		cp+=3;
		wmprintf("%c%c%c%c", Base64[a], Base64[b], Base64[c], Base64[d]);
		dst+=4;
		linelen+=4;
		if (linelen>=76) {
			wmprintf("\r\n");
			linelen=0;
		}
	}
	if (remlen==0) {
		wmprintf("\r\n");
	} else if (remlen==1) {
		a=(cp[0]>>2);
		b=(cp[0]<<4)&0x30;
		wmprintf("%c%c==\r\n", Base64[a], Base64[b]);
		dst+=4;
	} else if (remlen==2) {
		a=(cp[0]>>2);
		b=(cp[0]<<4)&0x30;
		b|=(cp[1]>>4);
		c=(cp[1]<<2)&0x3c;
		wmprintf("%c%c%c=\r\n", Base64[a], Base64[b], Base64[c]);
		dst+=4;
	}
	return dst;
}

int DecodeBase64(char *src, char *ctype)
{
	char dest[1024];
	int destidx, state, ch;
	int szdest;
	char *pos;

	memset(dest, 0, sizeof(dest));
	szdest=sizeof(dest)-1;
	state=0;
	destidx=0;
	while ((ch=*src++)!='\0') {
		if (isspace(ch)) continue;
		if (ch=='=') break;
		pos=strchr(Base64, ch);
		if (pos==0) return (-1);
		switch (state) {
			case 0:
				if (dest) {
					if (destidx>=szdest) return (-1);
					dest[destidx]=(pos-Base64)<<2;
				}
				state=1;
				break;
			case 1:
				if (dest) {
					if (destidx+1>=szdest) return (-1);
					dest[destidx]|=(pos-Base64)>>4;
					dest[destidx+1]=((pos-Base64)&0x0f)<<4;
				}
				destidx++;
				state=2;
				break;
			case 2:
				if (dest) {
					if (destidx+1>=szdest) return (-1);
					dest[destidx]|=(pos-Base64)>>2;
					dest[destidx+1]=((pos-Base64)&0x03)<<6;
				}
				destidx++;
				state=3;
				break;
			case 3:
				if (dest) {
					if (destidx>=szdest) return (-1);
					dest[destidx]|=(pos-Base64);
				}
				destidx++;
				state=0;
				break;
		}
	}
	if (strncasecmp(ctype, "text/html", 9)==0) {
		DecodeHTML(0, dest, ctype, 0);
	} else if (strncasecmp(ctype, "text/plain", 10)==0) {
		printf("%s\r\n", dest);
	} else {
		fwrite(dest, sizeof(char), destidx, stdout);
	}
	return (szdest);
}

int webmailheader(wmheader *header)
{
	char inbuffer[1024];
	char *pTemp;
	int  is_need_line = 1;

	for (;;) {
		if (is_need_line) { 
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket); 
			striprn(inbuffer);
		}
		if (strcmp(inbuffer, "")==0) break;
		if (strncasecmp(inbuffer, "From:", 5)==0) {
			pTemp=inbuffer+5;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->From, pTemp, sizeof(header->From)-1);
		}
		if (strncasecmp(inbuffer, "Replyto:", 8)==0) {
			pTemp=inbuffer+8;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->Replyto, pTemp, sizeof(header->Replyto)-1);
		}
	 	if (strncasecmp(inbuffer, "To:", 3)==0) {
			pTemp=inbuffer+3;
			strncpy(header->To, pTemp, sizeof(header->To)-1);
			int is_need_line = 1;
			while ( is_need_line ) {
   				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket); 
				striprn(inbuffer);
				if (strcmp(inbuffer, "")==0) break; // break from main for.
                     		pTemp=inbuffer;
				// yahoo used to put two spaces...
				if( (*pTemp=='\t') || (*pTemp==' ') ) {
					while ( (*pTemp==' ') || (*pTemp=='\t') ) pTemp++;
					strncat(header->To, pTemp, sizeof(header->To)-1-strlen(header->To));
				} else { 
					is_need_line  = 0;
				}
			}
		}
		if (strncasecmp(inbuffer, "Subject:", 8)==0) {
			pTemp=inbuffer+8;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->Subject, pTemp, sizeof(header->Subject)-1);
		}
		if (strncasecmp(inbuffer, "Date:", 5)==0) {
			pTemp=inbuffer+5;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->Date, pTemp, sizeof(header->Date)-1);
		}
//		if (strncasecmp(inbuffer, "Content-Type:", 13)==0) {
//			pTemp=inbuffer+13;
//			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
//			strncpy(header->contenttype, pTemp, sizeof(header->contenttype)-1);
//			if (strcasestr(header->contenttype, "multipart")==NULL) continue;
//			if (strcasestr(header->contenttype, "boundary=")==NULL) {
//				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
//				striprn(inbuffer);
//				if (strcasestr(inbuffer, "boundary=")!=NULL) {
//					strncat(header->contenttype, inbuffer, sizeof(header->contenttype)-strlen(header->contenttype)-1);
//				} else {
//					continue;
//				}
//			}
//		}
		if (strncasecmp(inbuffer, "Content-Type:", 13)==0) {
			pTemp=inbuffer+13;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->contenttype, pTemp, sizeof(header->contenttype)-1);
			if (strcasestr(header->contenttype, "multipart")==NULL) continue;
			int is_need_line = 1;
			while ( is_need_line ) {
   				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket); 
				striprn(inbuffer);
				if (strcmp(inbuffer, "")==0) break; // break from main for.
                     		pTemp=inbuffer;
				if( (*pTemp=='\t') || (*pTemp==' ') ) {
					while ( (*pTemp==' ') || (*pTemp=='\t') ) pTemp++;
					strncat(header->contenttype, inbuffer, sizeof(header->contenttype)-strlen(header->contenttype)-1);
				} else { 
					is_need_line  = 0;
				}
			}
		}
		if (strncasecmp(inbuffer, "Content-Transfer-Encoding: ", 26)==0) {
			pTemp=inbuffer+26;
			while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
			strncpy(header->encoding, pTemp, sizeof(header->encoding)-1);
		}
	}
	if ((pTemp=strcasestr(header->contenttype, "boundary="))!=NULL) {
		pTemp+=9;
		if (*pTemp=='\"') pTemp++;
		snprintf(header->boundary, sizeof(header->boundary)-1, "--");
		while ((*pTemp)&&(*pTemp!='\"')&&(strlen(header->boundary)<sizeof(header->boundary)-1)) {
			header->boundary[strlen(header->boundary)]=*pTemp;
			pTemp++;
		}
	}
	if (strlen(header->Replyto)==0) {
		pTemp=header->From;
		while ((*pTemp)&&(*pTemp!='<')) pTemp++;
		if (*pTemp=='<') pTemp++;
		while ((*pTemp)&&(*pTemp!='>')&&(strlen(header->Replyto)<sizeof(header->Replyto)-1)) {
			header->Replyto[strlen(header->Replyto)]=*pTemp;
			pTemp++;
		}
		if (strlen(header->Replyto)==0) {
			strncpy(header->Replyto, header->From, sizeof(header->Replyto)-1);
		}
	}
	if (strlen(header->From)==0) strcpy(header->From, "(No Sender Name)");
	if (strlen(header->Subject)==0) strcpy(header->Subject, "(No Subject)");
	if (strlen(header->Date)==0) strcpy(header->Date, "(No Date)");
	return 0;
}

#ifdef RAW_MESSAGES
void webmailraw()
{
	char inbuffer[1024];
	int nummessages;
	int nummessage;

	send_header(0, 200, "OK", "1", "text/plain", -1, -1);
	if (wmserver_connect()!=0) return;
	nummessages=wmserver_count();
	nummessage=atoi(getgetenv("MSG"));
	if ((nummessage>nummessages)||(nummessage<1)) {
		printf("%s<BR>", ERR_NOMESSAGE);
		wmserver_disconnect();
		return;
	}
	wmserver_msgretr(nummessage);
	for (;;) {
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strcmp(inbuffer, "")==0) break;
		printf("%s\r\n", inbuffer);
	}
	printf("\r\n");
	for (;;) {
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (is_msg_end(inbuffer)) break;
		printf("%s\r\n", inbuffer);
	}
	wmserver_disconnect();
	return;
}
#endif

void webmailfiledl()
{
	wmheader header;
	char *pQueryString;
	char *pTemp;
	char contentencoding[512];
	char contentfilename[512];
	char filename[512];
	char inbuffer[1024];
	char msgtype[100];
	int nummessages;
	int nummessage=0;

	memset(filename, 0, sizeof(filename));
	pQueryString=strstr(request.RequestURI, "/mailfile/");
	if (pQueryString==NULL) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/mailread?msg=%d\">\n", request.ScriptName, nummessage);
		printf("%s", ERR_BADURI);
		wmexit();
	}
	pQueryString+=10;
	nummessage=atoi(pQueryString);
	while ((isdigit(*pQueryString)!=0)&&(*pQueryString!=0)) pQueryString++;
	while (*pQueryString=='/') pQueryString++;
	strncpy(filename, pQueryString, sizeof(filename)-1);
	pTemp=filename;
	URLDecode(pTemp);
	if ((nummessage<1)||(strlen(filename)<1)) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/mailread?msg=%d\">\n", request.ScriptName, nummessage);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_INVALIDURI);
		wmexit();
	}
	if (wmserver_connect()!=0) return;
	nummessages=wmserver_count();
	if (nummessages<nummessage) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/mailread?msg=%d\">\n", request.ScriptName, nummessage);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOMESSAGE);
		goto quit;
	}
	memset(msgtype, 0, sizeof(msgtype));
	wmserver_msgretr(nummessage);
	memset((char *)&header, 0, sizeof(header));
	if (webmailheader(&header)!=0) return;
	if (strcasestr(header.contenttype, "multipart")==NULL) {
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) break;
		}
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/mailread?msg=%d\">\n", request.ScriptName, nummessage);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOFILES);
		goto quit;
	}
	for (;;) {
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (is_msg_end(inbuffer)) break;
		if (strncasecmp(inbuffer, header.boundary, strlen(header.boundary))==0) {
			memset(contentencoding, 0, sizeof(contentencoding));
			memset(contentfilename, 0, sizeof(contentfilename));
		}
		pTemp=strcasestr(inbuffer, "name=");
		if ((pTemp!=NULL)&&(strlen(contentfilename)==0)) {
			pTemp+=5;
			if (*pTemp=='\"') pTemp++;
			while ((*pTemp)&&(*pTemp!='\"')&&(strlen(contentfilename)<sizeof(contentfilename)-1)) {
				contentfilename[strlen(contentfilename)]=*pTemp;
				pTemp++;
			}
		}
		if (strncasecmp(inbuffer, "Content-Transfer-Encoding:", 26)==0) {
			strncpy(contentencoding, (char *)&inbuffer+27, sizeof(contentencoding)-1);
			if ((strncasecmp(contentencoding, "base64", 6)!=0)&&(strncasecmp(contentencoding, "quoted-printable", 16)!=0)&&
			    (strncasecmp(contentencoding, "7bit", 4)!=0)&&(strncasecmp(contentencoding, "8bit", 4)!=0)) {
				memset(contentencoding, 0, sizeof(contentencoding));
				memset(contentfilename, 0, sizeof(contentfilename));
				continue;
			}
		}
		if ((strlen(contentencoding))&&(strlen(contentfilename))) {
			if (strcmp(contentfilename, filename)!=0) {
				memset(contentencoding, 0, sizeof(contentencoding));
				memset(contentfilename, 0, sizeof(contentfilename));
				continue;
			}
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (is_msg_end(inbuffer)) goto quit;
				if (strcmp(inbuffer, "")==0) break;
			}
			if (strcmp(inbuffer, "")==0) break;
		}
	}
	if ((strlen(contentencoding))&&(strlen(contentfilename))) {
		printf("Content-Disposition: attachment\r\n");
		send_header(1, 200, "OK", "1", get_mime_type(contentfilename), -1, -1);
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) goto quit;
			if (strcasestr(inbuffer, header.boundary)!=NULL) break;
			if (strncasecmp(contentencoding, "base64", 6)==0) {
				DecodeBase64(inbuffer, "");
			} else if (strncasecmp(contentencoding, "quoted-printable", 16)==0) {
				DecodeQP(0, inbuffer, "");
			} else if (strncasecmp(contentencoding, "7bit", 4)==0) {
				DecodeText(0, inbuffer);
			} else if (strncasecmp(contentencoding, "8bit", 4)==0) {
				DecodeText(0, inbuffer);
			}
		}
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) goto quit;
		}
	} else {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/mailread?msg=%d\">\n", request.ScriptName, nummessage);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOFILE);
		goto quit;
	}
quit:
	wmserver_disconnect();
	return;
}

char *webmailfileul(char *xfilename, char *xfilesize)
{
	char *filebody=NULL;
	char lfilename[1024];
	int filesize=0;
	char line[1024];
	char location[1024];
	char boundary[1024];
	char boundary2[1024];
	char boundary3[1024];
	char *pPostData;
	char *pTemp;
	int i;
	unsigned int j;

	pPostData=request.PostData;
	memset(boundary, 0, sizeof(boundary));
	memset(location, 0, sizeof(location));
	i=0;
	j=0;
	/* duhh..  this should just retrieve the boundary from request.ContentType,
	 * and maybe check same to make sure this really is MIME, while it's at it.
	 */
	/* get the mime boundary */
	while ((*pPostData!='\r')&&(*pPostData!='\n')&&(i<request.ContentLength)&&(strlen(boundary)<sizeof(boundary)-1)) {
		boundary[j]=*pPostData;
		pPostData++;
		i++;
		j++;
	}
	/* eat newline garbage */
	while ((*pPostData=='\n')||(*pPostData=='\r')) {
		pPostData++;
		i++;
	}
	snprintf(boundary2, sizeof(boundary2)-1, "%s--", boundary);
	snprintf(boundary3, sizeof(boundary3)-1, "\r\n%s", boundary);
	pPostData=request.PostData;
	while ((strcmp(line, boundary2)!=0)&&(i<request.ContentLength)) {
		memset(line, 0, sizeof(line));
		j=0;
		while ((*pPostData!='\r')&&(*pPostData!='\n')&&(i<request.ContentLength)&&(strlen(line)<sizeof(line)-1)) {
			line[j]=*pPostData;
			pPostData++;
			i++;
			j++;
		}
		/* eat newline garbage */
		while ((*pPostData=='\n')||(*pPostData=='\r')) {
			pPostData++;
			i++;
		}
		pTemp=line;
		if (strncasecmp(line, "Content-Disposition: form-data; ", 32)!=0) continue;
		pTemp+=32;
		if (strncasecmp(pTemp, "name=\"fattach\"; ", 16)!=0) continue;
		pTemp+=16;
		if (strncasecmp(pTemp, "filename=\"", 10)!=0) continue;
		pTemp+=10;
		if (strrchr(pTemp, '\\')!=NULL) {
			pTemp=strrchr(pTemp, '\\')+1;
		}
		snprintf(lfilename, sizeof(lfilename)-1, "%s", pTemp);
		while (lfilename[strlen(lfilename)-1]=='\"') lfilename[strlen(lfilename)-1]='\0';
		while ((strncmp(pPostData, "\r\n\r\n", 4)!=0)&&(strncmp(pPostData, "\n\n", 2)!=0)&&(i<request.ContentLength)) {
			pPostData++;
			i++;
		}
		if (strncmp(pPostData, "\r\n\r\n", 4)==0) {
			pPostData+=4;
			i+=4;
		} else if (strncmp(pPostData, "\n\n", 2)==0) {
			pPostData+=2;
			i+=2;
		} else {
			continue;
		}
		snprintf(xfilename, 1024, "%s", lfilename);
		filebody=pPostData;
		filesize=0;
		while ((strncmp(pPostData, boundary3, strlen(boundary3))!=0)&&(i<request.ContentLength)) {
			pPostData++;
			filesize++;
		}
	}
	snprintf(xfilesize, 9, "%d", filesize);
	return filebody;
}

int retardedOEmime(short int reply, char *ctype)
{
	char *pTemp;
	char boundary[100];
	char inbuffer[1024];
	char msgencoding[100];
	char msgtype[100];
	int msgdone=0;

	memset(boundary, 0, sizeof(boundary));
	memset(msgtype, 0, sizeof(msgtype));
	memset(msgencoding, 0, sizeof(msgencoding));
	for (;;) {
		if (*ctype) {
			memcpy(inbuffer, ctype, strlen(ctype));
			*ctype='\0';
		} else {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			striprn(inbuffer);
		}
		if (strcmp(inbuffer, "")==0) break;
		if (strncasecmp(inbuffer, "Content-Type:", 13)==0) {
			strncpy(msgtype, (char *)&inbuffer+14, sizeof(msgtype)-1);
			if (strcasestr(msgtype, "multipart")==NULL) continue;
                        if (strcasestr(msgtype, "multipart/alternative")!=NULL) {
                                msgdone=retardedOEmime(0, inbuffer);
                                return msgdone;
                        }
			if (strcasestr(msgtype, "boundary=")==NULL) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				striprn(inbuffer);
				if (strcasestr(inbuffer, "boundary=")!=NULL) {
					strncat(msgtype, inbuffer, sizeof(msgtype)-strlen(msgtype)-1);
				} else {
					continue;
				}
			}
		}
	}
	pTemp=strcasestr(msgtype, "boundary=");
	if (pTemp!=NULL) {
		pTemp+=9;
		if (*pTemp=='\"') pTemp++;
		while ((*pTemp)&&(*pTemp!='\"')&&(strlen(boundary)<sizeof(boundary)-1)) {
			boundary[strlen(boundary)]=*pTemp;
			pTemp++;
		}
	}
	memset(msgtype, 0, sizeof(msgtype));
	memset(msgencoding, 0, sizeof(msgencoding));
	for (;;) {
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		striprn(inbuffer);
		if (strlen(msgtype)) {
			if (strcmp(inbuffer, "")==0) break;
		}
		if (strncasecmp(inbuffer, "Content-Type:", 13)==0) {
			strncpy(msgtype, (char *)&inbuffer+14, sizeof(msgtype)-1);
			if (strcasestr(msgtype, "multipart")==NULL) continue;
		}
		if (strncasecmp(inbuffer, "Content-Transfer-Encoding:", 26)==0) {
			strncpy(msgencoding, (char *)&inbuffer+27, sizeof(msgencoding)-1);
		}
	}
	if (strncasecmp(msgtype, "text/plain", 10)==0||strncasecmp(msgtype, "text/html", 9)==0) {
		if ((!reply)&&(strncasecmp(msgtype, "text/html", 9)!=0)) {
			printf("<PRE><FONT FACE=Arial, Verdana>");
		}
		for (;;) {
			msgdone=1;
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (strcasestr(inbuffer, boundary)!=NULL) return msgdone;
			if (strncasecmp(msgencoding, "quoted-printable", 16)==0) {
				DecodeQP(reply, inbuffer, msgtype);
			} else if (strncasecmp(msgencoding, "base64", 6)==0) {
				DecodeBase64(inbuffer, msgtype);
			} else if (strncasecmp(msgtype, "text/html", 9)==0) {
				DecodeText(reply, inbuffer);
			} else {
				printline(reply, inbuffer);
			}
		}
		if ((!reply)&&(strncasecmp(msgtype, "text/html", 9)!=0)) {
			printf("</FONT></PRE>\n<BR>\n");
		}
	}
	return msgdone;
}

void webmailmime(wmheader *header, int nummessage, short int reply)
{
	char filename[20][100];
	char inbuffer[1024];
	char ctype[100];
	char ccode[100];
	char *pTemp;
	int file=0;
	int head=0;
	int msgdone=0;
	int numfiles=0;
	int i;

	memset(filename, 0, sizeof(filename));
	if (strcasestr(header->contenttype, "multipart")==NULL) {
		if ((!reply)&&(strncasecmp(header->contenttype, "text/html", 9)!=0)) {
			printf("<PRE><FONT FACE=Arial, Verdana>");
		}
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) break;
			if (strncasecmp(header->encoding, "quoted-printable", 16)==0) {
				DecodeQP(reply, inbuffer, header->contenttype);
			} else if (strncasecmp(header->encoding, "base64", 6)==0) {
				DecodeBase64(inbuffer, header->contenttype);
			} else if (strncasecmp(header->contenttype, "text/html", 9)==0) {
				DecodeHTML(reply, inbuffer, header->contenttype, 1);
			} else {
				printline(reply, inbuffer);
			}
		}
		if ((!reply)&&(strncasecmp(header->contenttype, "text/html", 9)!=0)) {
			printf("</FONT></PRE>\n<BR>\n");
		}
		return;
	}
	for (;;) {
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (is_msg_end(inbuffer)) break;
		if (strcasestr(inbuffer, header->boundary)!=NULL) {
			head=1;
			break;
		}
	}
	for (;;) {
		if (is_msg_end(inbuffer)) break;
		if (head) {
			memset(ctype, 0, sizeof(ctype));
			memset(ccode, 0, sizeof(ccode));
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (is_msg_end(inbuffer)) break;
				if (strcmp(inbuffer, "")==0) {
					head=0;
					break;
				}
				if (strncasecmp(inbuffer, "Content-Transfer-Encoding:", 26)==0) {
					pTemp=inbuffer+26;
					while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
					strncpy(ccode, pTemp, sizeof(ccode)-1);
				}
				if (strncasecmp(inbuffer, "Content-Type:", 13)==0) {
					pTemp=inbuffer+13;
					while ((*pTemp==' ')||(*pTemp=='\t')) pTemp++;
					strncpy(ctype, pTemp, sizeof(ctype)-1);
				}
				if (strcasestr(header->contenttype, "multipart")==NULL) continue;
				if (strcasestr(ctype, "multipart/alternative")!=NULL) {
					msgdone=retardedOEmime(0, inbuffer);
					head=1;
					break;
				}
				if (file) continue;
				pTemp=strcasestr(inbuffer, "name=");
				if (pTemp!=NULL) {
					pTemp+=5;
					if (*pTemp=='\"') pTemp++;
					while ((*pTemp)&&(*pTemp!='\"')&&(strlen(filename[numfiles])<sizeof(filename[numfiles])-1)) {
						filename[numfiles][strlen(filename[numfiles])]=*pTemp;
						pTemp++;
					}
					file=1;
					numfiles++;
				}
			}
		}
		if (is_msg_end(inbuffer)) break;
		if (file) {
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (is_msg_end(inbuffer)) break;
				if (strcmp(inbuffer, "")==0) {
					file=0;
					break;
				}
				if (strcasestr(inbuffer, header->boundary)!=NULL) {
					file=0;
					head=1;
					break;
				}
			}
		}
		if (is_msg_end(inbuffer)) break;
		if (head) continue;
		// start reading the part body
		for (;;) {
			if (strcmp(inbuffer, "")==0) break;
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) break;
		}
		if (is_msg_end(inbuffer)) break;
		if (!reply) {
			if ((strncasecmp(header->contenttype, "multipart/report", 16)==0)||(strncasecmp(ctype, "message/rfc822", 14)==0)) {
				printf("<HR>");
			}
			if (strncasecmp(ctype, "text/html", 9)!=0) {
				printf("<PRE><FONT FACE=Arial, Verdana>");
			}
		}
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (is_msg_end(inbuffer)) break;
			if (strcasestr(inbuffer, header->boundary)!=NULL) {
				head=1;
				break;
			}
			if ((msgdone)&&(strncasecmp(header->contenttype, "multipart/report", 16)!=0)&&(strncasecmp(ctype, "message/rfc822", 14)!=0)) {
				continue;
			}
			if ((reply)&&(strncasecmp(ctype, "text/html", 9)!=0)&&(strncasecmp(ctype, "text/plain", 10)!=0)) {
				continue;
			}
			if (strncasecmp(ctype, "text/html", 9)==0) {
				if (strncasecmp(ccode, "quoted-printable", 16)==0) {
					DecodeQP(reply, inbuffer, ctype);
				} else if (strncasecmp(ccode, "base64", 6)==0) {
					DecodeBase64(inbuffer, ctype);
				} else {
					DecodeHTML(reply, inbuffer, ctype, 1);
				}
			} else if (strncasecmp(header->contenttype, "multipart/alternative", 21)!=0) {
				if (strncasecmp(ccode, "quoted-printable", 16)==0) {
					DecodeQP(reply, inbuffer, ctype);
				} else if (strncasecmp(ccode, "base64", 6)==0) {
					DecodeBase64(inbuffer, ctype);
				} else {
					printline(reply, inbuffer);
				}
			}
		}
		if ((!reply)&&(strncasecmp(ctype, "text/html", 9)!=0)) {
			printf("</FONT></PRE>\n<BR>\n");
		}
		if (strncasecmp(ctype, "text/html", 9)==0) msgdone=1;
		if ((strncasecmp(ctype, "text/plain", 10)==0)&&(strncasecmp(header->contenttype, "multipart/alternative", 21)!=0)) {
			msgdone=1;
		}
		if (is_msg_end(inbuffer)) break;
	}
	if ((!reply)&&(numfiles>0)) {
		printf("Attachments<BR>\n");
		for (i=0;i<numfiles;i++) {
			printf("[<A HREF=%s/mailfile/%d/", request.ScriptName, nummessage);
			printhex("%s", filename[i]);
			printf(">");
			printht("%s", filename[i]);
			printf("</A>]<BR>\n");
		}
	}
	return;
}

void webmaillist()
{
	wmheader header;
	char *pTemp;
	char inbuffer[1024];
	char msgsize[20];
	char uidl[100];
	signed char bgtoggle=0;
	int msize;
	int nummessages;
	int offset=0;
	int i;

	printheader();
	if (wmserver_connect()!=0) return;
	nummessages=wmserver_count();
	if (nummessages<1) {
		printf("<CENTER>%s</CENTER><BR><BR>\n", YWC_NOMAIL);
		printf("</BODY>\n</HTML>\n");
		wmserver_disconnect();
		return;
	}
	if ((pTemp=getgetenv("OFFSET"))!=NULL) {
		offset=atoi(pTemp);
	}
	if (offset<0) offset=0;
	printf("<script language='JavaScript'>\n<!--\n");
	printf("function CheckAll()\n{\n");
	printf("	for (var i=0;i<document.webmail.elements.length;i++) {\n");
	printf("		var e=document.webmail.elements[i];\n");
	printf("		if (e.name!='allbox') e.checked=!e.checked;\n");
	printf("	}\n");
	printf("}\n//-->\n</script>\n");
	if (nummessages>MAX_LIST_SIZE) {
		printf("<CENTER>\n");
		if (offset>0) {
			printf("[<A HREF=%s/maillist?offset=%d>%s</A>]\n", request.ScriptName, offset-MAX_LIST_SIZE, YWC_PREVIOUS);
		} else {
			printf("[%s]\n", YWC_PREVIOUS);
		}
		if (offset+MAX_LIST_SIZE<nummessages) {
			printf("[<A HREF=%s/maillist?offset=%d>%s</A>]\n", request.ScriptName, offset+MAX_LIST_SIZE, YWC_NEXT);
		} else {
			printf("[%s]\n", YWC_NEXT);
		}
		printf("</CENTER>\n");
	}
	printf("<CENTER>\n<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=1 WIDTH=90%%>\n");
	printf("<FORM METHOD=POST NAME=webmail ACTION=%s/maildelete>\n", request.ScriptName);
	printf("<TR BGCOLOR=%s>\n", COLOR_TRIM);
	printf("<TH ALIGN=LEFT>&nbsp;</TH>");
	printf("<TH ALIGN=LEFT width=90%%><FONT COLOR=%s>%s</FONT></TH>", COLOR_TRIMTEXT, YWC_FROM);
	printf("<TH ALIGN=LEFT WIDTH=90%%><FONT COLOR=%s>%s</FONT></TH>", COLOR_TRIMTEXT, YWC_SUBJECT);
	printf("<TH ALIGN=LEFT WIDTH=90%%><FONT COLOR=%s>%s</FONT></TH>", COLOR_TRIMTEXT, YWC_DATE);
	printf("<TH ALIGN=LEFT WIDTH=90%%><FONT COLOR=%s>%s</FONT></TH>", COLOR_TRIMTEXT, YWC_SIZE);
	printf("<TH ALIGN=LEFT>&nbsp;</TH>");
	printf("</TR>\n");
	for (i=nummessages-offset-1;(i>-1)&&(i>nummessages-offset-MAX_LIST_SIZE-1);i--) {
		if ((msize=wmserver_msgsize(i+1))<0) continue;
		if (msize>=1048576) {
			snprintf(msgsize, sizeof(msgsize)-1, "%1.1f M", (float)msize/1048576.0);
		} else {
			snprintf(msgsize, sizeof(msgsize)-1, "%1.1f K", (float)msize/1024.0);
		}
		memset(uidl, 0, sizeof(uidl));
		wmserver_uidl(i+1, uidl);
		wmserver_msghead(i+1);
		memset((char *)&header, 0, sizeof(header));
		if (webmailheader(&header)!=0) return;
		if (strncasecmp(wmservertype, "pop3", 4)==0) {
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (strcmp(inbuffer, ".")==0) break;
			}
		} else if (strncasecmp(wmservertype, "imap", 4)==0) {
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (isdigit(inbuffer[0])) break;
			}
		}
		if ((pTemp=strchr(header.From, '<'))!=NULL) *pTemp='\0';
		if ((bgtoggle=abs(bgtoggle-1))==0) {
			printf("<TR BGCOLOR=#D0D0D0>");
		} else {
			printf("<TR BGCOLOR=#E0E0E0>");
		}
		printf("<TD NOWRAP><INPUT TYPE=checkbox NAME=%d VALUE=\"%s\"></TD>", i+1, uidl);
		printf("<TD NOWRAP>%.28s&nbsp;</TD>", DecodeRFC2047(header.From));
		printf("<TD NOWRAP><A HREF=%s/mailread?msg=%d ALT=\"%s\">", request.ScriptName, i+1, DecodeRFC2047(header.Subject));
		printf("%.45s</A>&nbsp;</TD>", DecodeRFC2047(header.Subject));
		printf("<TD NOWRAP>%s&nbsp;</TD>", wmgetdate(header.Date));
		printf("<TD ALIGN=RIGHT NOWRAP>%s&nbsp;</TD>", msgsize);
		if (strcasestr(header.contenttype, "multipart/mixed")!=NULL) {
			printf("<TD><IMG BORDER=0 SRC=%spaperclip.png HEIGHT=16 WIDTH=11 ALT='%s'></TD>", BASE_IMAGE_URL, YWC_FILE);
		} else {
			printf("<TD>&nbsp;</TD>");
		}
		printf("</TR>\n");
	}
	printf("<TR><TD ALIGN=center COLSPAN=6>\n");
	printf("<INPUT TYPE=checkbox NAME=allbox VALUE=check_all onclick='CheckAll();'>\n");
	printf("<B>%s</B><BR>\n", YWC_SELECTALL);
	printf("<INPUT class=\"login\" TYPE=SUBMIT VALUE='%s' />\n", YWC_DELSELECTED);
	printf("</TD></TR>\n");
	printf("</FORM>\n");
	printf("</TABLE>");
	if (nummessages>MAX_LIST_SIZE) {
		printf("<CENTER>\n");
		if (offset>0) {
			printf("[<A HREF=%s/maillist?offset=%d>%s</A>]\n", request.ScriptName, offset-MAX_LIST_SIZE, YWC_PREVIOUS);
		} else {
			printf("[%s]\n", YWC_PREVIOUS);
		}
		if (offset+MAX_LIST_SIZE<nummessages) {
			printf("[<A HREF=%s/maillist?offset=%d>%s</A>]\n", request.ScriptName, offset+MAX_LIST_SIZE, YWC_NEXT);
		} else {
			printf("[%s]\n", YWC_NEXT);
		}
		printf("</CENTER>\n");
	}
	printf("</BODY>\n</HTML>\n");
	wmserver_disconnect();
	return;
}

void webmailread()
{
	wmheader header;
	char uidl[100];
	int nummessages;
	int nummessage;

	printheader();
	if (wmserver_connect()!=0) return;
	nummessages=wmserver_count();
	nummessage=atoi(getgetenv("MSG"));
	if ((nummessage>nummessages)||(nummessage<1)) {
		printf("%s<BR>", ERR_NOMESSAGE);
		wmserver_disconnect();
		return;
	}
	memset(uidl, 0, sizeof(uidl));
	wmserver_uidl(nummessage, uidl);
	printf("<SCRIPT LANGUAGE=JavaScript>\n<!--\n");
	printf("function ConfirmDelete() {\n");
	printf("	return confirm(\"Are you sure you want to delete this message?\");\n");
	printf("}\n");
	printf("// -->\n</SCRIPT>\n");
	printf("<CENTER>\n");
	if (nummessage>1) {
		printf("[<A HREF=%s/mailread?msg=%d>%s</A>]\n", request.ScriptName, nummessage-1, YWC_PREVIOUS);
	} else {
		printf("[%s]\n", YWC_PREVIOUS);
	}
	printf("[<A HREF=%s/mailwrite?replyto=%d>%s</A>]\n", request.ScriptName, nummessage, YWC_REPLY);
	printf("[<A HREF=%s/mailwrite?forward=%d>%s</A>]\n", request.ScriptName, nummessage, YWC_FORWARD);
	printf("[<A HREF=%s/maildelete?%d=%s onClick=\"return ConfirmDelete();\">%s</A>]\n", request.ScriptName, nummessage, uidl, YWC_DELETE);
	if (nummessage<nummessages) {
		printf("[<A HREF=%s/mailread?msg=%d>%s</A>]\n", request.ScriptName, nummessage+1, YWC_NEXT);
	} else {
		printf("[%s]\n", YWC_NEXT);
	}
	printf("<BR><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=1 WIDTH=90%%>\n");
	wmserver_msgretr(nummessage);
	memset((char *)&header, 0, sizeof(header));
	if (webmailheader(&header)!=0) return;
	printf("<TR><TH ALIGN=LEFT BGCOLOR=%s><FONT COLOR=%s>&nbsp;%s&nbsp;&nbsp;</FONT></TH><TD BGCOLOR=%s WIDTH=88%%><A HREF=%s/mailwrite?msgto=%s>", COLOR_TRIM, COLOR_TRIMTEXT, YWC_FROM, COLOR_FTEXT, request.ScriptName, header.Replyto);
	printht("%s", DecodeRFC2047(header.From));
	printf("</A></TD></TR>\n");
	printf("<TR><TH ALIGN=LEFT BGCOLOR=%s><FONT COLOR=%s>&nbsp;%s&nbsp;&nbsp;</FONT></TH><TD BGCOLOR=%s WIDTH=88%%>", COLOR_TRIM, COLOR_TRIMTEXT, YWC_TO, COLOR_FTEXT);
	printht("%s", DecodeRFC2047(header.To));
	printf("</TD></TR>\n");
	printf("<TR><TH ALIGN=LEFT BGCOLOR=%s><FONT COLOR=%s>&nbsp;%s&nbsp;&nbsp;</FONT></TH><TD BGCOLOR=%s WIDTH=88%%>", COLOR_TRIM, COLOR_TRIMTEXT, YWC_SUBJECT, COLOR_FTEXT);
	printht("%s", DecodeRFC2047(header.Subject));
	printf("</TD></TR>\n");
	printf("<TR><TH ALIGN=LEFT BGCOLOR=%s><FONT COLOR=%s>&nbsp;%s&nbsp;&nbsp;</FONT></TH><TD BGCOLOR=%s WIDTH=88%%>", COLOR_TRIM, COLOR_TRIMTEXT, YWC_DATE, COLOR_FTEXT);
	printht("%s", header.Date);
	printf("</TD></TR>\n");
#ifdef RAW_MESSAGES
	printf("<TR BGCOLOR=%s><TD COLSPAN=2>[<A HREF=%s/mailraw?msg=%d TARGET=_blank>%s</A>]</TD></TR>\n", COLOR_FTEXT, request.ScriptName, nummessage, YWC_VIEW_SRC);
#endif
	printf("<TR BGCOLOR=%s><TD COLSPAN=2>\n", COLOR_FTEXT);
	webmailmime(&header, nummessage, 0);
	printf("</TD></TR></TABLE>\n");
	if (nummessage>1) {
		printf("[<A HREF=%s/mailread?msg=%d>%s</A>]\n", request.ScriptName, nummessage-1, YWC_PREVIOUS);
	} else {
		printf("[%s]\n", YWC_PREVIOUS);
	}
	printf("[<A HREF=%s/mailwrite?replyto=%d>%s</A>]\n", request.ScriptName, nummessage, YWC_REPLY);
	printf("[<A HREF=%s/mailwrite?forward=%d>%s</A>]\n", request.ScriptName, nummessage, YWC_FORWARD);
	printf("[<A HREF=%s/maildelete?%d=%s onClick=\"return ConfirmDelete();\">%s</A>]\n", request.ScriptName, nummessage, uidl, YWC_DELETE);
	if (nummessage<nummessages) {
		printf("[<A HREF=%s/mailread?msg=%d>%s</A>]\n", request.ScriptName, nummessage+1, YWC_NEXT);
	} else {
		printf("[%s]\n", YWC_NEXT);
	}
	printf("<BR><BR>\n");
	printf("</BODY>\n</HTML>\n");
	wmserver_disconnect();
	return;
}

void webmailwrite()
{
	wmheader header;
	char msgto[512];
	char subject[512];
	int replyto=0;
	int forward=0;

	printheader();
	if (wmserver_connect()!=0) return;
	memset(msgto, 0, sizeof(msgto));
	memset(subject, 0, sizeof(subject));
	if (getgetenv("REPLYTO")!=NULL) {
		replyto=atoi(getgetenv("REPLYTO"));
	}
	if (getgetenv("FORWARD")!=NULL) {
		forward=atoi(getgetenv("FORWARD"));
	}
	if (getgetenv("MSGTO")!=NULL) {
		strncpy(msgto, getgetenv("MSGTO"), sizeof(msgto)-1);
	}
	if ((replyto>0)||(forward>0)) {
		if (replyto>0) {
			wmserver_msgretr(replyto);
		} else {
			wmserver_msgretr(forward);
		}
		memset((char *)&header, 0, sizeof(header));
		if (webmailheader(&header)!=0) return;
		snprintf(msgto, sizeof(msgto)-1, "%s", header.Replyto);
		if (replyto>0) {
			if (strncasecmp(DecodeRFC2047(header.Subject), "RE:", 3)!=0) {
				snprintf(subject, sizeof(subject)-1, "Re: %s", DecodeRFC2047(header.Subject));
			} else {
				snprintf(subject, sizeof(subject)-1, "%s", DecodeRFC2047(header.Subject));
			}
		} else if (forward>0) {
			snprintf(subject, sizeof(subject)-1, "Fwd: %s", DecodeRFC2047(header.Subject));
		}
	}
	printf("<CENTER>\n");
	printf("<FORM METHOD=POST ACTION=%s/mailsend NAME=wmcompose ENCTYPE=multipart/form-data>\n", request.ScriptName);
	printf("<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0>\n");
	printf("<TR BGCOLOR=%s><TD><B>&nbsp;%s&nbsp;</B></TD><TD><INPUT class=\"login\" TYPE=TEXT NAME=msgto VALUE=\"%s\" SIZE=60></TD></TR>\n", COLOR_EDITFORM, YWC_TO, str2html(msgto));
	printf("<TR BGCOLOR=%s><TD><B>&nbsp;%s&nbsp;</B></TD><TD><INPUT class=\"login\" TYPE=TEXT NAME=msgsubject VALUE=\"%s\" SIZE=60></TD></TR>\n", COLOR_EDITFORM, YWC_SUBJECT, str2html(subject));
	printf("<TR BGCOLOR=%s><TD><B>&nbsp;%s&nbsp;</B></TD><TD><INPUT class=\"login\" TYPE=TEXT NAME=msgcc VALUE=\"\" SIZE=60></TD></TR>\n", COLOR_EDITFORM, YWC_CC);
	printf("<TR BGCOLOR=%s><TD><B>&nbsp;%s&nbsp;</B></TD><TD><INPUT class=\"login\" TYPE=TEXT NAME=msgbcc VALUE=\"\" SIZE=60></TD></TR>\n", COLOR_EDITFORM, YWC_BCC);
	printf("<TR BGCOLOR=%s><TD><B>&nbsp;%s&nbsp;</B></TD><TD><INPUT class=\"login\" TYPE=FILE NAME=fattach SIZE=60 /></TD></TR>\n", COLOR_EDITFORM, YWC_FILE);
	printf("<TR BGCOLOR=%s><TD COLSPAN=2><TEXTAREA class=\"login\" NAME=msgbody COLS=80 ROWS=20 WRAP=PHYSICAL>\n", COLOR_EDITFORM);
	if ((replyto<1)&&(forward<1)) {
		printf("</TEXTAREA></TD></TR>\n");
		printf("</TABLE>\n");
		printf("<INPUT class=\"login\" TYPE=SUBMIT VALUE='%s' />\n", YWC_SENDMAIL);
		printf("</FORM>\n</CENTER>\n");
		printf("<SCRIPT LANGUAGE=JavaScript>\n<!--\ndocument.wmcompose.msgto.focus();\n// -->\n</SCRIPT>\n");
		printf("</BODY>\n</HTML>\n");
		wmserver_disconnect();
		return;
	}
	printf(YWC_REPLYLINE, header.From);
	if (forward>0) {
		printf(YWC_FWD_FROM, header.From);
		printf(YWC_FWD_SUBJECT, header.Subject);
		printf(YWC_FWD_DATE, header.Date);
		printf(">\n");
	}
	if (replyto>0) {
		webmailmime(&header, replyto, 1);
	} else {
		webmailmime(&header, forward, 1);
	}
	printf("</TEXTAREA></TD></TR>\n");
	printf("</TABLE>\n");
	printf("<INPUT TYPE=SUBMIT VALUE='%s'>\n", YWC_SENDMAIL);
	printf("</FORM>\n</CENTER>\n");
	printf("<SCRIPT LANGUAGE=JavaScript>\n<!--\ndocument.wmcompose.msgto.focus();\n// -->\n</SCRIPT>\n");
	printf("</BODY>\n</HTML>\n");
	wmserver_disconnect();
	return;
}

void webmailsend()
{
	char *filebody=NULL;
	char *msgbody=NULL;
	char *pmsgbody;
	char *ptemp;
	char boundary[100];
	char inbuffer[1024];
	char filename[1024];
	char cfilesize[10];
	char msgaddr[128];
	char msgto[1024];
	char msgcc[1024];
	char msgbcc[1024];
	char msgsubject[128];
	char msgdate[100];
	char msgdatetz[100];
	char line[1024];
	char wmaddress[1024];
	int filesize=0;
#ifdef WIN32
	struct timeb tstruct;
#else
	struct timeval ttime;
	struct timezone tzone;
#endif
	time_t t;
	int i;

	t=time((time_t*)0);
	memset(boundary, 0, sizeof(boundary));
	snprintf(boundary, sizeof(boundary)-1, "------------YWC%d", (int)t);
	if (strcasecmp(request.RequestMethod, "POST")!=0) return;
	printheader();
#ifdef APPEND_ADDRESS
	snprintf(wmaddress, sizeof(wmaddress)-1, "%s%s", wmusername, APPEND_ADDRESS);
#else
	strncpy(wmaddress, wmusername, sizeof(wmaddress)-1);
	if (strchr(wmusername, '@')==NULL) {
		unsigned int i, count;

		strncat(wmaddress, "@", sizeof(wmaddress)-strlen(wmaddress)-1);
		ptemp=wmpop3server;
		do {
			for (i=0, count=0;i<strlen(ptemp);i++) if (ptemp[i]=='.') count++;
			if (count>1) ptemp=strchr(ptemp, '.')+1;
		} while (count>1);
		strncat(wmaddress, ptemp, sizeof(wmaddress)-strlen(wmaddress)-1);
	}
#endif
	while ((wmaddress[strlen(wmaddress)-1]=='\r')||(wmaddress[strlen(wmaddress)-1]=='\n')) {
		wmaddress[strlen(wmaddress)-1]='\0';
	}
	if (getmimeenv("MSGTO")==NULL) {
		printf("<CENTER>%s</CENTER>\n", ERR_NORECIPIENT);
		return;
	}
	memset(msgto, 0, sizeof(msgto));
	memset(msgcc, 0, sizeof(msgcc));
	memset(msgbcc, 0, sizeof(msgbcc));
	memset(msgsubject, 0, sizeof(msgsubject));
	if (msgbody!=NULL) {
		free(msgbody);
		msgbody=NULL;
	}
	msgbody=calloc(request.ContentLength+1024, sizeof(char));
	if (getmimeenv("MSGTO")!=NULL)
		strncpy(msgto, getmimeenv("MSGTO"), sizeof(msgto)-1);
	if (getmimeenv("MSGCC")!=NULL)
		strncpy(msgcc, getmimeenv("MSGCC"), sizeof(msgcc)-1);
	if (getmimeenv("MSGBCC")!=NULL)
		strncpy(msgbcc, getmimeenv("MSGBCC"), sizeof(msgbcc)-1);
	if (getmimeenv("MSGSUBJECT")!=NULL)
		strncpy(msgsubject, getmimeenv("MSGSUBJECT"), sizeof(msgsubject)-1);
	if (getmimeenv("MSGBODY")!=NULL)
		strncpy(msgbody, getmimeenv("MSGBODY"), request.ContentLength+1023);
	memset(filename, 0, sizeof(filename));
	memset(cfilesize, 0, sizeof(cfilesize));
	if (getmimeenv("FATTACH")!=NULL) {
		filebody=webmailfileul(filename, cfilesize);
		filesize=atoi(cfilesize);
		if (strlen(filename)==0) filesize=0;
	}
#ifdef WIN32
	ftime(&tstruct);
	strftime(msgdate, sizeof(msgdate), "%a, %d %b %Y %H:%M:%S", localtime(&tstruct.time));
	snprintf(msgdatetz, sizeof(msgdatetz)-1, " %+.4d", -tstruct.timezone/60*100);
	strncat(msgdate, msgdatetz, sizeof(msgdate)-strlen(msgdate)-1);
#else
	gettimeofday(&ttime, &tzone);
	strftime(msgdate, sizeof(msgdate), "%a, %d %b %Y %H:%M:%S", localtime(&ttime.tv_sec));
	snprintf(msgdatetz, sizeof(msgdatetz)-1, " %+.4d", -tzone.tz_minuteswest/60*100);
	strncat(msgdate, msgdatetz, sizeof(msgdate)-strlen(msgdate)-1);
#endif
	if ((strlen(wmusername)==0)||(strlen(wmpassword)==0)||(strlen(wmpop3server)==0)||(strlen(wmsmtpserver)==0)) return;
	if (wmserver_smtpconnect()<0) return;
	wmprintf("HELO %s\r\n", wmsmtpserver);
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(inbuffer, "250", 3)!=0) goto quit;
#ifdef SMTP_AUTH
		if (wmserver_smtpauth()!=0) goto quit;
#endif
	wmprintf("MAIL From: <%s>\r\n", wmaddress);
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(inbuffer, "250", 3)!=0) goto quit;
	printf(YWC_SMTP_FROMOK, wmaddress);
	for (i=0;i<3;i++) {
		switch (i) {
			case 1: ptemp=msgcc; break;
			case 2: ptemp=msgbcc; break;
			default: ptemp=msgto; break;
		}
		while (*ptemp) {
			if (strstr(ptemp, "@")==NULL) break;
			memset(msgaddr, 0, sizeof(msgaddr));
			while ((*ptemp)&&(*ptemp!=',')&&(*ptemp!=' ')&&(strlen(ptemp)<sizeof(msgaddr)-1)) {
				msgaddr[strlen(msgaddr)]=*ptemp;
				ptemp++;
			}
			while ((*ptemp==',')||(*ptemp==' ')) {
				ptemp++;
			}
			while (!isalpha(msgaddr[strlen(msgaddr)-1])) {
				msgaddr[strlen(msgaddr)-1]='\0';
			}
			wmprintf("RCPT To: <%s>\r\n", msgaddr);
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (strncasecmp(inbuffer, "250", 3)!=0) goto quit;
			printf("Recipient '%s' OK.<BR>\n", msgaddr);
		}
	}
	wmprintf("DATA\r\n");
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(inbuffer, "354", 3)!=0) goto quit;
	wmprintf("From: %s <%s>\r\n", wmusername, wmaddress);
	wmprintf("To: %s\r\n", msgto);
	if (strlen(msgcc)) {
		wmprintf("Cc: %s\r\n", msgcc);
	}
	wmprintf("Subject: %s\r\n", msgsubject);
	wmprintf("Date: %s\r\n", msgdate);
	if (filesize>0) {
		wmprintf("MIME-Version: 1.0\r\n");
		wmprintf("Content-Type: multipart/mixed; boundary=\"%s\"\r\n", boundary);
	}
	wmprintf("X-Mailer: %s\r\n", YWC_SERVERNAME);
	wmprintf("\r\n");
	pmsgbody=msgbody;
	if (filesize>0) {
		wmprintf("%s\r\n", YWC_MIME);
		wmprintf("--%s\r\n", boundary);
		wmprintf("Content-Type: text/plain\r\n");
		wmprintf("Content-Transfer-Encoding: 8bit\r\n\r\n");
	}
        // The textarea in Internet Explorer has a width of 79
        // The textarea in Google Chrome has a width of 82
        // The standard says the line SHOULD be no more than 78 characters, excluding the CRLF
        //
        // I have opted for a maximum line length of 86, excluding the CRLF (in part to cope with different width text areas)
	//   however, would not normally expect the line to be longer than 82 characters.
        //
        while (strlen(pmsgbody)>82) {   // aim for 82
                memset(line, 0, sizeof(line));
                snprintf(line, 86, "%s", pmsgbody);
                while (strlen(line)>0) {
                        if (line[strlen(line)-1]==' ') {
                                line[strlen(line)-1]='\0';
                                wmprintf("%s\r\n", line);
                                pmsgbody+=strlen(line);
                                break;
                        }
                        if (line[strlen(line)-1]=='\r') {
                                if (line[strlen(line)]=='\n') {
                                        line[strlen(line)-1]='\0';
                                        wmprintf("%s\r\n", line);
                                        pmsgbody+=strlen(line)+2;
                                        break;
                                }
                                else
                                {
                                        wmprintf("%s\r\n", line);
                                        pmsgbody+=strlen(line)+1;
                                        break;
                                }
                        }
                        line[strlen(line)-1]='\0';
                } // while strlen(line)<86.
                if (strlen(line)==0) {
			//truncate the line as it runs for over 80 characters without any convenint place to break
                        snprintf(line, 80, "%s", pmsgbody);
                        wmprintf("%s\r\n", line);
                        pmsgbody+=80;
                }
        }  //pmsgbody > 82
        memset(line, 0, sizeof(line));
        snprintf(line, 83, "%s", pmsgbody);  // 82 chatracters from pmsgbody + terminating '/0'
        wmprintf("%s\r\n", line);
        free(msgbody);
	if (filesize>0) {
		printf(YWC_SENDINGFILE, filename, filesize);
		fflush(stdout);
		wmprintf("\r\n--%s\r\n", boundary);
		wmprintf("Content-Type: application/octet-stream; name=\"%s\"\r\n", filename);
		wmprintf("Content-Transfer-Encoding: base64\r\n");
		wmprintf("Content-Disposition: attachment; filename=\"%s\"\r\n\r\n", filename);
		EncodeBase64(filebody, filesize);
		wmprintf("\r\n--%s--\r\n", boundary);
	}
	wmprintf("\r\n.\r\n");
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(inbuffer, "250", 3)==0) {
		printf("<BR>%s<BR>\n", YWC_SMTP_SENT);
	}
	memset(inbuffer, 0, sizeof(inbuffer));
	printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/maillist\">\n", request.ScriptName);
quit:
	printf("%s<BR>\n", inbuffer);
	printf("</BODY>\n</HTML>\n");
	wmserver_smtpdisconnect();
	return;
}

void webmaildelete()
{
	char msgnum[8];
	char uidl[100];
	int nummessages;
	int deleted=0;
	int i;

	printheader();
	if (wmserver_connect()!=0) return;
	nummessages=wmserver_count();
	if (nummessages>0) {
		for (i=nummessages;i>0;i--) {
			snprintf(msgnum, sizeof(msgnum)-1, "%d", i);
			if ((getpostenv(msgnum)==NULL)&&(getgetenv(msgnum)==NULL)) continue;
			memset(uidl, 0, sizeof(uidl));
			wmserver_uidl(i, uidl);
			if ((getpostenv(msgnum)!=NULL)&&(strcmp(getpostenv(msgnum), uidl)!=0)) continue;
			if ((getgetenv(msgnum)!=NULL)&&(strcmp(getgetenv(msgnum), uidl)!=0)) continue;
			printf(YWC_DELETING, i);
			deleted=i;
			if (wmserver_msgdele(i)==0) {
				printf("%s<BR>\n", YWC_DELETE_OK);
			} else {
				printf("%s<BR>\n", YWC_DELETE_BAD);
			}
			fflush(stdout);
		}
	} else {
		printf("<CENTER>%s</CENTER><BR>\n", YWC_NOMAIL);
	}
	wmserver_disconnect();
	snprintf(msgnum, sizeof(msgnum)-1, "%d", deleted);
	if (deleted>=nummessages) deleted=nummessages-1;
	printf("<SCRIPT LANGUAGE=JavaScript>\n<!--\n");
	printf("location.replace(\"");
	if ((getgetenv(msgnum)==NULL)||(nummessages<2)) {
		printf("%s/maillist\");\n", request.ScriptName);
	} else {
		printf("%s/mailread?msg=%d\");\n", request.ScriptName, deleted);
	}
	printf("// -->\n</SCRIPT>\n");
	printf("<NOSCRIPT>\n");
	printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; ");
	if ((getgetenv(msgnum)==NULL)||(nummessages<2)) {
		printf("URL=%s/maillist\">\n", request.ScriptName);
	} else {
		printf("URL=%s/mailread?msg=%d\">\n", request.ScriptName, deleted);
	}
	printf("</NOSCRIPT>\n");
	printf("</BODY>\n</HTML>\n");
	return;
}
