/*
    ywcmail -- simple pop3/smtp web e-mail cgi
    Copyright (C) 2002  Chan Yin Chi < 0330@bigfoot.com >

*/
#include "main.h"

void printheader()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
	printf("<HTML><HEAD><TITLE>%s</TITLE>\n", YWC_SERVERNAME);
#ifdef STYLESHEET
	printf("<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"%s\">", STYLESHEET);
#else
	printf("<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"%s\">", STYLESHEET);
#ifndef USE_FRAMES
	printf(".TBAR\n");
	printf("{\n");
	printf("<style></style>\n");
	printf("}\n");
#endif
	printf("</STYLE>\n");
#endif
	printf("</HEAD>\n");
	printf("<BODY BGCOLOR=%s COLOR=#000000 LINK=%s ALINK=%s VLINK=%s LEFTMARGIN=0 TOPMARGIN=0 MARGINHEIGHT=0 MARGINWIDTH=0>\n", COLOR_TRIMTEXT, COLOR_LINKS, COLOR_LINKS, COLOR_LINKS);
#ifndef USE_FRAMES
	printf("<CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=100%%>\n");
	printf("<TR VALIGN=middle BGCOLOR=#C0C0C0><TD ALIGN=left>&nbsp;\n");
	printf("<a class='TBAR' href=%s/maillist>%s</a>&nbsp;&middot;&nbsp;<BR>", request.ScriptName, YWC_TOP_INBOX);
	printf("<a class='TBAR' href=%s/mailwrite>%s</a>&nbsp;&middot;&nbsp;<BR>", request.ScriptName, YWC_TOP_COMPOSE);
	printf("<a class='TBAR' href=%s/mailquit TARGET=_top>%s</a>\n<BR>", request.ScriptName, YWC_TOP_LOGOUT);
	printf("</TD></TR></TABLE>\n");
	printf("</CENTER>");
#endif
	printf("<BR>\n");
	return;
}

void printlogin()
{
	if (strlen(wmpop3server)<1) strncpy(wmpop3server, POP3_HOST, sizeof(wmpop3server)-1);
	if (strlen(wmsmtpserver)<1) strncpy(wmsmtpserver, SMTP_HOST, sizeof(wmsmtpserver)-1);
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
	printf("<HTML>\n<HEAD><TITLE>%s</TITLE>\n", LOGIN_TITLE);
	printf("<META HTTP-EQUIV=\"Description\" CONTENT=\"%s - Copyright (c) 2002 Ying Wa College - http://www.yingwa.edu.hk/\">\r\n", YWC_SERVERNAME);
#ifdef STYLESHEET
	printf("<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"%s\">", STYLESHEET);
#else
	printf("<STYLE>\n");
	printf(".JUSTIFY { text-align: justify }\n");
	printf("A { color: %s; text-decoration: none }\n", COLOR_LINKS);
	printf("</STYLE>\n");
#endif
	printf("</HEAD>\n");
	printf("<Body bgcolor=#131F61 topmargin=0 leftmargin=0 marginwidth=0 marginheight=0> \n");
	printf("<Table border=0 cellspacing=0 cellpadding=0 width=\"700\"> \n");
	printf("<tr> \n");
	printf("<td BGCOLOR=\"#E6E6E6\"><img SRC=\"/webmail/spacer.gif\" height=74 width=231></td> \n\n");
	printf("<td ROWSPAN=\"4\"><img SRC=\"/webmail/ywcweb01.jpg\" height=367 width=13></td> \n\n");
	printf("<td COLSPAN=\"4\"><img SRC=\"/webmail/ywcweb02.jpg\" height=74 width=456></td> \n");
	printf("</tr> \n\n");
	printf("<tr> \n");
	printf("<td ROWSPAN=\"3\"><img SRC=\"/webmail/ywcweb03.jpg\" height=293 width=231></td> \n\n");
	printf("<td><img SRC=\"/webmail/ywcweb04.jpg\" height=185 width=68></td> \n\n");
	printf("<td><img SRC=\"/webmail/spacer.gif\" height=185 width=81></td> \n\n");
	printf("<td><form METHOD=POST ACTION=\"%s/\" NAME=wmlogin>", request.ScriptName);
	printf("<b><font face=\"Arial\"><font color=\"#FFFFFF\"><font size=+0>%s</font></font></font></b>\n", LOGIN_USERNAME);
	printf("<input class=\"login\" NAME=WMUSERNAME TYPE=TEXT SIZE=20 VALUE=\"%s\">\n", wmusername);
	printf("<br><b><font face=\"Arial\"><font color=\"#FFFFFF\"><font size=+0>%s</font></font></font></b>\n", LOGIN_PASSWORD);
	printf("<input class=\"login\" NAME=WMPASSWORD TYPE=PASSWORD SIZE=20> \n");
#ifndef SUPPORT_IMAP
	printf("<br><input NAME=WMSERVERTYPE TYPE=HIDDEN VALUE=\"POP3\">\n");
#endif
#ifndef SUPPORT_POP3
	printf("<br><input NAME=WMSERVERTYPE TYPE=HIDDEN VALUE=\"IMAP\">\n");
#endif
#ifdef SUPPORT_POP3
#ifdef SUPPORT_IMAP
	printf("<B>Server Type</B><BR>\n");
	printf("<SELECT NAME=WMSERVERTYPE>\n");
	printf("<OPTION%s>IMAP\n", strcmp(wmservertype, "IMAP")==0?" SELECTED":"");
	printf("<OPTION%s>POP3\n", strcmp(wmservertype, "POP3")==0?" SELECTED":"");
	printf("</SELECT><BR>\n");
#endif
#endif
#ifndef NO_USER_HOSTS
	printf("<B>%s</B><BR>\n", LOGIN_POP3HOST);
	printf("<INPUT NAME=WMPOP3SERVER TYPE=TEXT SIZE=20 VALUE=\"%s\"><BR>\n", wmpop3server);
	printf("<B>%s</B><BR>\n", LOGIN_SMTPHOST);
	printf("<INPUT NAME=WMSMTPSERVER TYPE=TEXT SIZE=20 VALUE=\"%s\"><BR>\n", wmsmtpserver);
#endif
	printf("<br><center>\n<input class=\"login\" TYPE=SUBMIT VALUE=Login /></center>\n");
	printf("</td>\n\n");
	printf("<td><img SRC=\"/webmail/spacer.gif\" height=185 width=152></td> \n");
	printf("</tr> \n\n");
	printf("<tr> \n");
	printf("<td><img SRC=\"/webmail/ywcweb06.jpg\" height=85 width=68></td> \n\n");
	printf("<td COLSPAN=\"3\"><img SRC=\"/webmail/ywcweb07.jpg\" height=85 width=388></td> \n");
	printf("</tr> \n\n");
	printf("<tr> \n");
	printf("<td><img SRC=\"/webmail/spacer.gif\" height=23 width=68></td> \n\n");
	printf("<td ALIGN=CENTER COLSPAN=\"3\"><b>");
	printf("<font face=\"Verdana\"><font color=\"#FFFFFF\"><font size=-1>2002 \n");
	printf("<a href=\"http://www.yingwa.edu.hk\"><font colour=\"orange\">Ying Wa College</a></font></font></font></font><b></td>");
	printf("</tr> \n");
	printf("</table> \n\n");
	printf("</body> \n");
	printf("</html> \n");
	return;
}

void printlogout()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
	printf("<HTML>\n<HEAD><TITLE>%s</TITLE>\n", LOGOUT_TITLE);
#ifdef STYLESHEET
	printf("<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"%s\">", STYLESHEET);
#else
	printf("<STYLE>\n");
	printf(".JUSTIFY { text-align: justify }\n");
	printf("A { color: %s; text-decoration: none }\n", COLOR_LINKS);
	printf("</STYLE>\n");
#endif
	printf("</HEAD>\n");
	printf("<BODY BGCOLOR=#F0F0F0 COLOR=#000000 LINK=%s ALINK=%s VLINK=%s>\n", COLOR_LINKS, COLOR_LINKS, COLOR_LINKS);
	printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL='%s'\">\n", request.ScriptName);
	printf("<CENTER>\n<BR><BR>\n");
	printf("<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>\n");
	printf("<TR BGCOLOR=%s><TH><FONT COLOR=%s>%s</FONT></TH></TR>\n", COLOR_TRIM, COLOR_TRIMTEXT, LOGOUT_TITLE);
	printf("<TR BGCOLOR=%s><TD>\n", COLOR_EDITFORM);
	printf("%s",LOGOUT_MESSAGE);
	printf("</TD></TR>\n");
	printf("</TABLE>\n");
	printf("</BODY>\n</HTML>\n");
}

#ifdef USE_FRAMES
void printframeset()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Frameset//EN\">\r\n");
	printf("<HTML><HEAD><TITLE>%s</TITLE></HEAD>\n", YWC_SERVERNAME);
	printf("<FRAMESET ROWS='80,*' BORDER=0 FRAMEBODER=0 FRAMESPACING=0>\n");
	printf("<FRAME SRC=%s/topframe NAME=top SCROLLING=NO NORESIZE>\n", request.ScriptName);
	printf("<FRAMESET COLS='115,*' BORDER=0 FRAMEBORDER=0 FRAMESPACING=0>\n");
	printf("<FRAME SRC=%s/leftframe NAME=left SCROLLING=NO NORESIZE>\n", request.ScriptName);
	printf("<FRAME SRC=%s/maillist NAME=main NORESIZE>\n", request.ScriptName);
	printf("</FRAMESET>\n</FRAMESET>\n");
	printf("<NOFRAMES>\n%s\n</NOFRAMES>\n", ERR_NOFRAMES);
	printf("</HTML>\n");
}

void printtopframe()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
	printf("<HTML><HEAD><TITLE>%s</TITLE></HEAD>\n", YWC_SERVERNAME);
	printf("<BODY BGCOLOR=%s LEFTMARGIN=5 TOPMARGIN=5 MARGINHEIGHT=0 MARGINWIDTH=0>\n", COLOR_TRIM);
	printf("<IMG SRC=%sheader.png BORDER=0>", BASE_IMAGE_URL);
	printf("</BODY>\n</HTML>\n");
}

void printleftframe()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
	printf("<HTML><HEAD><TITLE>%s</TITLE>\n", YWC_SERVERNAME);
	printf("<SCRIPT LANGUAGE='JavaScript'>\n");
	printf("<!--\n");
	printf("if (document.images) {\n");
	printf("image1on = new Image();\n");
	printf("image1on.src = '%smenuinbox1.png';\n", BASE_IMAGE_URL);
	printf("image2on = new Image();\n");
	printf("image2on.src = '%smenucompose1.png';\n", BASE_IMAGE_URL);
	printf("image3on = new Image();\n");
	printf("image3on.src = '%smenulogout1.png';\n", BASE_IMAGE_URL);
	printf("image1off = new Image();\n");
	printf("image1off.src = '%smenuinbox0.png';\n", BASE_IMAGE_URL);
	printf("image2off = new Image();\n");
	printf("image2off.src = '%smenucompose0.png';\n", BASE_IMAGE_URL);
	printf("image3off = new Image();\n");
	printf("image3off.src = '%smenulogout0.png';\n", BASE_IMAGE_URL);
	printf("}\n");
	printf("function changeImages() {\n");
	printf("if (document.images) {\n");
	printf("for (var i=0; i<changeImages.arguments.length; i+=2) {\n");
	printf("document[changeImages.arguments[i]].src = eval(changeImages.arguments[i+1] + '.src');\n");
	printf("}\n}\n}\n");
	printf("// -->\n");
	printf("</SCRIPT>\n");
	printf("</HEAD>\n");
	printf("<BODY BGCOLOR=%s COLOR=#000000 LINK=#FFFFFF ALINK=#FFFFFF VLINK=#FFFFFF LEFTMARGIN=0 TOPMARGIN=5 MARGINHEIGHT=0 MARGINWIDTH=0>\n", COLOR_TRIM);
	printf("<CENTER>\n");
	printf("<A HREF=%s/maillist STYLE='color: FFFFFF; text-decoration: none; font-size=10pt' TARGET=main ONMOUSEOVER=\"changeImages('image1', 'image1on')\" ONMOUSEOUT=\"changeImages('image1', 'image1off')\">\n", request.ScriptName);
	printf("<IMG NAME=image1 SRC=%smenuinbox0.png BORDER=0></A><BR><B><font color=\"FFFFFF\"><font size=\"-1\">%s</font></font></B><BR><BR><BR>\n", BASE_IMAGE_URL, YWC_TOP_INBOX);
	printf("<A HREF=%s/mailwrite STYLE='color: FFFFFF; text-decoration: none; font-size=10pt' TARGET=main ONMOUSEOVER=\"changeImages('image2', 'image2on')\" ONMOUSEOUT=\"changeImages('image2', 'image2off')\">\n", request.ScriptName);
	printf("<IMG NAME=image2 SRC=%smenucompose0.png BORDER=0></A><BR><B><font color=\"FFFFFF\"><font size=\"-1\">%s</font></font></B><BR><BR><BR>\n", BASE_IMAGE_URL, YWC_TOP_COMPOSE);
	printf("<A HREF=%s/mailquit STYLE='color: FFFFFF; text-decoration: none; font-size=10pt' TARGET=_top ONMOUSEOVER=\"changeImages('image3', 'image3on')\" ONMOUSEOUT=\"changeImages('image3', 'image3off')\">\n", request.ScriptName);
	printf("<IMG NAME=image3 SRC=%smenulogout0.png BORDER=0></A><BR><B><font color=\"FFFFFF\"><font size=\"-1\">%s</font></font></B><BR><BR><BR>\n", BASE_IMAGE_URL, YWC_TOP_LOGOUT);
	printf("</CENTER>\n</BODY>\n</HTML>\n");
}
#endif

void sig_timeout()
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<B>Connection timed out.</B>\n");
#ifndef WIN32
#ifdef USE_SYSLOG
	closelog();
#endif
#endif
	exit(0);
}

void sig_catchint(int sig)
{
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	printf("<B>Caught unhandled signal %d.</B>\n", sig);
#ifndef WIN32
#ifdef USE_SYSLOG
	closelog();
#endif
#endif
	exit(0);
}

void setsigs()
{
	short int i;
#ifdef _NSIG
	short int numsigs=_NSIG;
#else
	short int numsigs=NSIG;
#endif

	for(i=0;i<numsigs;i++) {
		signal(i, sig_catchint);
	}
#ifdef WIN32
	SetErrorMode(SEM_FAILCRITICALERRORS|SEM_NOGPFAULTERRORBOX|SEM_NOOPENFILEERRORBOX);
#else
	signal(SIGALRM, sig_timeout);
	alarm(MAX_RUNTIME);
#endif
}

//  First stage of login
//
//
int ywc_login( char* username, char * password)
{
	if (wm_is_user_allowed( username ) >0)
	{
		//"Login as this user not allowed.");
        	return 0;
	}

	// is user valid in etc/passwd
	int r = 0;
	//local login without POP3 LOGON
	//r = login(username, password);

	if ( r!=0 )
	{
		//"allowed users ok but system login_failed" );
		// attempt virtual login
		// read through list of usernames in VIRTUAL_USER_DB
		// is virtual account disabled ?
		// does password match ?
	}
	return r;
}

int main(int argc, char *argv[])
{
	if (getenv("REQUEST_METHOD")==NULL) {
		printf("This program must be run as a CGI.\n");
		exit(0);
	}
	setsigs();
	setvbuf(stdout, NULL, _IONBF, 0);
#ifdef WIN32
	_setmode(_fileno(stdin), _O_BINARY);
	_setmode(_fileno(stdout), _O_BINARY);
#else
#ifdef USE_SYSLOG
	openlog("ywcmail", LOG_PID, LOG_MAIL);
#endif
#endif
	connected=0;
	cgi_readenv();
	if (strncmp(request.RequestURI, "/mailfile", 9)==0) {
		wmcookieget();
		webmailfiledl();
		fflush(stdout);
		return 0;
	}
#ifdef USE_FRAMES
	if (strncmp(request.RequestURI, "/topframe", 9)==0) {
		printtopframe();
		fflush(stdout);
		return 0;
	} else if (strncmp(request.RequestURI, "/leftframe", 10)==0) {
		printleftframe();
		fflush(stdout);
		return 0;
	}
#endif
	if ((strcmp(request.RequestURI, "/")==0)&&(strcmp(request.RequestMethod, "POST")==0)) {
                // logon screen form submission
                char user[1024];
                char pass[1024];

                // passed in through POST from the environment
                if (getpostenv("WMUSERNAME")!=NULL) {
                        strncpy(user, getpostenv("WMUSERNAME"), sizeof(user)-1);
                }else {
                        //"WMUSERNAME, is blank " );
                }
                if (getpostenv("WMPASSWORD")!=NULL) {
                        strncpy(pass, getpostenv("WMPASSWORD"), sizeof(pass)-1);
                }else {
                        //"WMPASSWORD, is blank " );
                }
                if (ywc_login(user, pass)==0) {
                        // system login successfull - authenticated against shadow
                        if (wmcookieset()==0) {
                                send_header(0, 200, "OK", "1", "text/html", -1, -1);
#ifdef USE_FRAMES
                                printframeset();
#else
                                webmaillist();
#endif
                        } else {
                                send_header(0, 200, "OK", "1", "text/html", -1, -1);
                                printlogin();
                        }
                }
                else
                {
                        printheader();
                        printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
                        printf("<CENTER>%s</CENTER><BR>\n", "Invalid Logon");
                }
                fflush(stdout);
                return 0;
        }
        wmcookieget();
#ifdef RAW_MESSAGES
	if (strncmp(request.RequestURI, "/mailraw", 8)==0) {
		webmailraw();
		fflush(stdout);
		return 0;
	}
#endif
	if (strcmp(request.RequestURI, "/")==0) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printlogin();
		fflush(stdout);
		return 0;
	}
	if (strncmp(request.RequestURI, "/mailquit", 9)==0) {
		wmcookiekill();
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printlogout();
		fflush(stdout);
		exit(0);
	}
	send_header(0, 200, "OK", "1", "text/html", -1, -1);
	if (strncmp(request.RequestURI, "/maillist", 9)==0) {
		webmaillist();
	} else if (strncmp(request.RequestURI, "/mailread", 9)==0) {
		webmailread();
	} else if (strncmp(request.RequestURI, "/mailwrite", 10)==0) {
		webmailwrite();
	} else if (strncmp(request.RequestURI, "/mailsend", 9)==0) {
		webmailsend();
	} else if (strncmp(request.RequestURI, "/maildelete", 11)==0) {
		webmaildelete();
	}
	fflush(stdout);
	if (request.PostData!=NULL) {
		free(request.PostData);
	}
#ifndef WIN32
#ifdef USE_SYSLOG
	closelog();
#endif
#endif
	return 0;
}
