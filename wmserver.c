/*
    ywcmail -- simple pop3/smtp web e-mail cgi
    Copyright (C) 2002  Chan Yin Chi < 0330@ bigfoot.com >
    code based on null webmail
    
*/
#include "main.h"
static struct sockaddr_in server;
static struct hostent *hp;
#ifdef WIN32
static WSADATA wsaData;
#endif
static char inbuffer[1024];
static char *ptemp;

/*
void logdata(const char *format, ...)
{
	char logbuffer[1024];
	char file[200];
	va_list ap;
	FILE *fp;

#ifdef WIN32
	snprintf(file, sizeof(file)-1, "C:\\webmail.log");
#else
	snprintf(file, sizeof(file)-1, "/tmp/webmail.log");
#endif
	fp=fopen(file, "a");
	if (fp!=NULL) {
		va_start(ap, format);
		vsnprintf(logbuffer, sizeof(logbuffer)-1, format, ap);
		va_end(ap);
		fprintf(fp, "%s", logbuffer);
		fclose(fp);
	}
}
*/

int wmprintf(const char *format, ...)
{
	char buffer[1024];
	va_list ap;

	va_start(ap, format);
	vsnprintf(buffer, sizeof(buffer)-1, format, ap);
	va_end(ap);
	send(wmsocket, buffer, strlen(buffer), 0);
//	logdata (">> %s", buffer);
	return 0;
}

int wmfgets(char *buffer, int max, int fd)
{
	char *pbuffer=buffer;
	char temp[2];
	int n=0;
	int rc;

	memset(temp, 0, sizeof(temp));
	while (n<max) {
		rc=recv(fd, temp, 1, 0);
		if (rc<1) {
			connected=0;
			wmexit();
		} else if (rc!=1) {
			n=-n;
			break;
		}
		n++;
		if (temp[0]==13) continue;
		*buffer=temp[0];
		buffer++;
		if (temp[0]==10) break;
	}
	*buffer=0;
	striprn(pbuffer);
//	logdata ("<< %s\r\n", pbuffer);
	return n;
}

void wmclose()
{
	char junk[10];

#ifdef WIN32
	shutdown(wmsocket, 2);
#endif
	while (recv(wmsocket, junk, sizeof(junk), 0)>0);
#ifdef WIN32
	closesocket(wmsocket);
#else
	close(wmsocket);
#endif
	fflush(stdout);
	return;
}

void wmexit()
{
	wmserver_disconnect();
	wmclose();
#ifdef WIN32
	WSACleanup();
#endif
	exit(0);
}

int wmserver_smtpconnect()
{
	/* some smtp servers like pop auth before smtp */
	if (wmserver_connect()!=0) return -1;
	wmserver_disconnect();
	if (!(hp=gethostbyname(wmsmtpserver))) {
		printf("<CENTER>%s '%s'</CENTER>\n", ERR_DNS_SMTP, wmsmtpserver);
		return -1;
	}
	memset((char *)&server, 0, sizeof(server));
	memmove((char *)&server.sin_addr, hp->h_addr, hp->h_length);
	server.sin_family=hp->h_addrtype;
	server.sin_port=(unsigned short)htons(SMTP_PORT);
	if ((wmsocket=socket(AF_INET, SOCK_STREAM, 0))<0) return -1;
//	setsockopt(wmsocket, SOL_SOCKET, SO_KEEPALIVE, 0, 0);
	if (connect(wmsocket, (struct sockaddr *)&server, sizeof(server))<0) {
		printf("<CENTER>%s '%s'</CENTER>\n", ERR_CON_SMTP, wmsmtpserver);
		return -1;
	}
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	return 0;
}

#ifdef SMTP_AUTH
int wmserver_smtpauth()
{
	char authstring[200];

	/* Thanks to Ryan Brown <ryan@wabdo.com> for the improved SMTP AUTH patch. */
	wmprintf("AUTH PLAIN ");
	snprintf(authstring, sizeof(authstring)-1, "%s%c%s%c%s", wmusername, '\0', wmusername, '\0', wmpassword);
	EncodeBase64(authstring, (strlen(wmusername)*2)+strlen(wmpassword)+2);
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(inbuffer, "502", 3)==0) return 0;
	if (strncasecmp(inbuffer, "235", 3)!=0) return -1;
	return 0;
}
#endif

void wmserver_smtpdisconnect()
{
	wmprintf("QUIT\r\n");
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	wmclose();
}

int wmserver_connect()
{
	if (connected) return 0;
	if ((strlen(wmusername)==0)||(strlen(wmpassword)==0)) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOUSER);
		wmexit();
	}
	if ((strlen(wmpop3server)==0)||(strlen(wmsmtpserver)==0)) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOHOST);
		wmexit();
	}
#ifdef WIN32
	if (WSAStartup(0x101, &wsaData)) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
		printf("<CENTER>%s</CENTER><BR>\n", ERR_NOWINSOCK);
		wmexit();
	}
#endif
	if (!(hp=gethostbyname(wmpop3server))) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
		printf("<CENTER>%s '%s'</CENTER><BR>\n", ERR_DNS_POP3, wmpop3server);
		wmexit();
	}
	memset((char *)&server, 0, sizeof(server));
	memmove((char *)&server.sin_addr, hp->h_addr, hp->h_length);
	server.sin_family=hp->h_addrtype;
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
		server.sin_port=htons(POP3_PORT);
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
		server.sin_port=htons(IMAP_PORT);
	}
	if ((wmsocket=socket(AF_INET, SOCK_STREAM, 0))<0) return -1;
	if (connect(wmsocket, (struct sockaddr *)&server, sizeof(server))<0) {
		send_header(0, 200, "OK", "1", "text/html", -1, -1);
		printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
		printf("<CENTER>%s '%s'</CENTER><BR>\n", ERR_CON_POP3, wmpop3server);
		wmexit();
	}
	connected=1;
	/* Check current status */
	wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		/* Send user name */
		wmprintf("USER %s\r\n", wmusername);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) {
			ptemp=strchr(inbuffer, ' ');
			send_header(0, 200, "OK", "1", "text/html", -1, -1);
			printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
			if (ptemp) printf("<CENTER>%s</CENTER><BR>\n", ptemp);
			return -1;
		}
		/* Send password */
		wmprintf("PASS %s\r\n", wmpassword);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) {
			ptemp=strchr(inbuffer, ' ');
			send_header(0, 200, "OK", "1", "text/html", -1, -1);
			printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
			if (ptemp) printf("<CENTER>%s</CENTER><BR>\n", ptemp);
			return -1;
		}
		return 0;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		/* Send user name */
		wmimapidx++;
		wmprintf("%d LOGIN %s %s\r\n", wmimapidx, wmusername, wmpassword);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		if (strncasecmp(ptemp, "OK", 2)!=0) {
			ptemp=strchr(inbuffer, ' ');
			send_header(0, 200, "OK", "1", "text/html", -1, -1);
			printf("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=%s/\">\n", request.ScriptName);
			if (ptemp) printf("<CENTER>%s</CENTER><BR>\n", ptemp);
			wmexit();
		}
		wmimapidx++;
		wmprintf("%d SELECT INBOX\r\n", wmimapidx);
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (isdigit(inbuffer[0])) break;
		}
		return 0;
#endif
	}
	return -1;
}

void wmserver_disconnect()
{
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		if (connected) {
			wmprintf("QUIT\r\n");
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			wmclose();
			connected=0;
		}
		return;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d LOGOUT\r\n", wmimapidx);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		wmclose();
		connected=0;
		return;
#endif
	}
	return;
}

int wmserver_count()
{
#ifdef SUPPORT_IMAP
	int count=0;
#endif

	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("STAT\r\n");
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		ptemp++;
		return atoi(ptemp);
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d STATUS INBOX (MESSAGES)\r\n", wmimapidx);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		count=atoi(ptemp);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		return count;
#endif
	}
	return -1;
}

int wmserver_msgdele(int message)
{
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("DELE %d\r\n", message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		return 0;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d STORE %d +FLAGS (\\Deleted)\r\n", wmimapidx, message);
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (isdigit(inbuffer[0])) break;
		}
		wmimapidx++;
		wmprintf("%d EXPUNGE\r\n", wmimapidx);
		for (;;) {
			wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
			if (isdigit(inbuffer[0])) break;
		}
		return 0;
#endif
	}
	return -1;
}

int wmserver_msghead(int message)
{
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("TOP %d 0\r\n", message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		return 0;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d FETCH %d BODY[HEADER]\r\n", wmimapidx, message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		return 0;
#endif
	}
	return -1;
}

int wmserver_msgretr(int message)
{
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("RETR %d\r\n", message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		return 0;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d FETCH %d RFC822\r\n", wmimapidx, message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		return 0;
#endif
	}
	return -1;
}

int wmserver_msgsize(int message)
{
#ifdef SUPPORT_IMAP
	int size=0;
#endif

	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("LIST %d\r\n", message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		ptemp++;
		return atoi(ptemp);
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d FETCH %d RFC822.SIZE\r\n", wmimapidx, message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		size=atoi(ptemp);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		return size;
#endif
	}
	return -1;
}

int wmserver_uidl(int message, char *uidl)
{
	static const char Base64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char uidltemp[71];
	unsigned char a, b, c, d;
	int dst, i, enclen, remlen;

	memset(uidltemp, 0, sizeof(uidltemp));
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		wmprintf("UIDL %d\r\n", message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		if (strncasecmp(inbuffer, "+OK", 3)!=0) return -1;
		ptemp=inbuffer;
		while ((*ptemp)&&(*ptemp!=' ')) ptemp++;
		ptemp++;
		while ((*ptemp)&&(*ptemp!=' ')) ptemp++;
		ptemp++;
		snprintf(uidltemp, 70, "%s", ptemp);
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		wmimapidx++;
		wmprintf("%d FETCH %d UID\r\n", wmimapidx, message);
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
		ptemp=inbuffer;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		while ((ptemp)&&(*ptemp!=' ')) ptemp++;
		if (*ptemp) ptemp++;
		snprintf(uidltemp, 70, "%s", ptemp);
		ptemp=uidltemp;
		while ((ptemp)&&(*ptemp!=')')) ptemp++;
		if (*ptemp) *ptemp='\0';
		wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
#endif
	}
	ptemp=uidltemp;
	dst=0;
	enclen=strlen(uidltemp)/3;
	remlen=strlen(uidltemp)-3*enclen;
	for (i=0;i<enclen;i++) {
		a=(ptemp[0]>>2);
	        b=(ptemp[0]<<4)&0x30;
		b|=(ptemp[1]>>4);
		c=(ptemp[1]<<2)&0x3c;
		c|=(ptemp[2]>>6);
		d=ptemp[2]&0x3f;
		ptemp+=3;
		uidl[dst++]=Base64[a];
		uidl[dst++]=Base64[b];
		uidl[dst++]=Base64[c];
		uidl[dst++]=Base64[d];
	}
	if (remlen==1) {
		a=(ptemp[0]>>2);
		b=(ptemp[0]<<4)&0x30;
		uidl[dst++]=Base64[a];
		uidl[dst++]=Base64[b];
	} else if (remlen==2) {
		a=(ptemp[0]>>2);
		b=(ptemp[0]<<4)&0x30;
		b|=(ptemp[1]>>4);
		c=(ptemp[1]<<2)&0x3c;
		uidl[dst++]=Base64[a];
		uidl[dst++]=Base64[b];
		uidl[dst++]=Base64[c];
	}
	return 0;
}

int is_msg_end(char *buffer)
{
	if (strncasecmp(wmservertype, "pop3", 4)==0) {
#ifdef SUPPORT_POP3
		if (strcmp(buffer, ".")==0) return 1;
#endif
	} else if (strncasecmp(wmservertype, "imap", 4)==0) {
#ifdef SUPPORT_IMAP
		if (strcmp(buffer, ")")==0) {
			for (;;) {
				wmfgets(inbuffer, sizeof(inbuffer)-1, wmsocket);
				if (isdigit(inbuffer[0])) break;
			}
			return 1;
		}
#endif
	}
	return 0;
}
